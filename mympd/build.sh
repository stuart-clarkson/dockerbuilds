#!/bin/bash

# Version just needs to be the numbers
# e.g. 6.6.1 (NOT v6.6.1)
VERSION=$1
ARCH=$(uname -m)

docker build -t "tras2/mympd:${VERSION}-${ARCH}" -t "tras2/mympd:latest" --build-arg VERSION=$VERSION .
docker push tras2/mympd:$VERSION-$ARCH

docker manifest create tras2/mympd:latest --amend tras2/mympd:$VERSION-$ARCH
docker manifest push tras2/mympd:latest

