#!/bin/bash

# Version just needs to be the numbers
# e.g. 6.6.1 (NOT v6.6.1)
VERSION=$1
ARCH=$(uname -m)

docker build -t "tras2/rx:${VERSION}-${ARCH}" -t "tras2/rx:${ARCH}-latest" --build-arg VERSION=$VERSION .
docker push tras2/rx:$VERSION-$ARCH
docker push tras2/rx:${ARCH}-latest


